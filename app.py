from collections import OrderedDict
import json
import traceback
from flask import Flask, render_template, Markup, request, jsonify
import optparse
from urllib.request import Request, urlopen
import glob
import os
import pandas as pd
from io import StringIO
from pathlib import Path
import re
import shutil


app = Flask(__name__)

app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config["CERTIFICATES_DROPBOX_PATH"] = str(Path.home()) + "/portfolios_dropbox/certificates/"
app.config["PHOTOS_DROPBOX_PATH"] = str(Path.home()) + "/portfolios_dropbox/Photos/"
app.config["ABOUT_PROGRAM_DROPBOX_PATH"] = str(Path.home()) + "/portfolios_dropbox/About Program/"
app.config["CERTIFICATES_PATH"] = str(Path.home()) + "/portfolios_los/certificates/"
app.config["PHOTOS_PATH"] = str(Path.home()) + "/portfolios_los/profile_images/"
app.config["JSON_PATH"] = str(Path.home()) + "/portfolios_los/profile_json/"
app.config["ABOUT_PROGRAM_PATH"] = str(Path.home()) + "/portfolios_los/about_program/"

with open('portfolioFields.json', 'r') as f:
    app.config["portfolioFields"] = json.load(f)

app.config["portfolioSampleData"] = OrderedDict([("name", "Achyuth Krishna Chepuri"), ("trainee_id", "2200571"), ("about_program_image", "VLSI_C2.JPG"), ("designation", "Software Developer"), ("company_name", "Bell Canada"), ("program_name", "PG Level Advanced Certification Programme in VLSI Chip Design"), ("cohort#", "Cohort 2"), ("program_code", "CDS"), ("description", "PG Level Advanced Certification Programme in VLSI Chip Design, is a 6 months hands-on programme by IISc Bangalore and TalentSprint. The programme is most trusted by the experienced professionals. For details, visit https://iisc.talentsprint.com/vlsi/faq.html"), ("project_titile", "NA"), ("project_description", "NA"), ("about_program", "The PG Level Advanced Certification Programme in VLSI Chip Design enables professionals to build VLSI chip designing capabilities that can power new-age technologies like AI, IoT, VR, Mobility, Cloud, and Analytics. The Department of Electronic Systems Engineering, IISc, with its pioneering and ongoing research and training in VLSI chip design, is best positioned to offer this programme. The programme is ideal for VLSI industry professionals who want to leverage expertise of modern tools and technologies."),("template_name", "aiml.html"), ("Hackathon1 AI/ML" , "Description of Hackathon1 AI/ML")])

@app.route('/cohorts/<batchName>')
def cohort_home(batchName):
    batch_list = []
    data = []
    try:
        for files in glob.glob("/home/dashboard/portfolios/cohorts/" + batchName + "/*.json"):
            read_file = open(files)
            data = data + json.load(read_file)
            batch_list.append(int(os.path.splitext(os.path.basename(files))[0]))
            batch_list.sort(reverse = True)
    except Exception as err:
            print(err)
            #return "Unable to load JSON file"
    #return data
    return render_template('index.html', data = data, batches = batch_list)

@app.route('/<rid>')
def home(rid):
    rid = str(rid)
    try:
        print(os.path.join(app.config["JSON_PATH"], f"{rid}.json"))
        with open(os.path.join(app.config["JSON_PATH"], f"{rid}.json"), "r") as json_file:
            data = json.load(json_file)
        # with urlopen('https://cdn.talentsprint.com/portfolios/profile_json/'+ rid +'.json') as url:
        #     data = json.loads(url.read().decode())
    except Exception as err:
        print(err)
        #return "Unable to find your Trainee ID!!"
        return render_template('page-not-found.html')
    # if "template" in data:
    #     return render_template(data["template"], data = data, rid = rid)

    return render_template('output.html', data = data, rid = rid)

def check_files(trainee_id, about_program_image):
    certificate_file = glob.glob(os.path.join(app.config["CERTIFICATES_DROPBOX_PATH"], str(trainee_id) + ".*"))
    
    profile_image_file = glob.glob(os.path.join(app.config["PHOTOS_DROPBOX_PATH"], str(trainee_id) + ".*"))
    
    about_prog_path = os.path.join(app.config["ABOUT_PROGRAM_DROPBOX_PATH"], about_program_image)
    
    server_image_path = os.path.join(app.config["ABOUT_PROGRAM_PATH"], about_program_image)

    print(os.path.exists(about_prog_path))

    if  not (len(certificate_file) > 0) or not (len(profile_image_file) > 0):
        return {}
    
    if not os.path.exists(server_image_path) and not os.path.exists(about_prog_path):
        return {}

    return {"certificate_file" : certificate_file[0], "profile_image_file" : profile_image_file[0], "about_prog_path" : about_prog_path if not os.path.exists(server_image_path) else "" }

def copy_files(trainee_id, about_program_image, program_code):

    files = check_files(trainee_id, about_program_image)
    
    if not files:
        return False

    dest_certificate_path = os.path.join(app.config["CERTIFICATES_PATH"], program_code)
    dest_image_path = os.path.join(app.config["PHOTOS_PATH"], program_code)

    os.makedirs(dest_certificate_path, exist_ok = True)
    os.makedirs(dest_image_path, exist_ok = True)

    if not ".pdf" in files["certificate_file"]:
        shutil.copy(files["certificate_file"], dest_certificate_path)

    shutil.copy(files["profile_image_file"], dest_image_path)

    if files["about_prog_path"]:
        shutil.copy(files["about_prog_path"], app.config["ABOUT_PROGRAM_PATH"])

    return True

def generate_trainee_data(data):
    trainee_id = data.get('trainee_id')
    about_program_image = data.get('about_program_image')

    regex = re.compile(r'(?P<Project>Project) (?P<project_number>\d+) (?P<Title>Title)$')

    projects_description = {}
    projects_list = []

    for key, value in data.items():
        matched_string = re.search(regex, key)
        if matched_string and value:
            project_num = matched_string.groupdict()
            desc_key = 'Project ' + project_num["project_number"] + ' Description'
            desc_value = data.get(desc_key)
            if desc_value:
                projects_description[value] = desc_value
                projects_list.append(value)

    data["project_list"] = projects_list
    data["project_description"] = projects_description

    return data

def get_trainee_data(trainee_id):
    trainee_file_path = os.path.join(app.config["JSON_PATH"], f"{trainee_id}.json")

    if not os.path.exists(trainee_file_path):
        return None

    with open(trainee_file_path, 'r') as trainee_file:
        trainee_data = json.load(trainee_file)

    return trainee_data

@app.route('/process_portfolios', methods=['POST', 'GET'])
def handle_trainee_data():
    data = request.json
    trainee_id = data.get('trainee_id', None)
    about_program_image = data.get('about_program_image', None)
    copy = int(request.args.get('edit', 0))

    if trainee_id and about_program_image:
        if copy == 1:
            if not copy_files(trainee_id, about_program_image, data.get("program_code")):
                response = {
                    "status": "Failure",
                    "message": "Portfolio for trainee_id: {0} is not generated".format(trainee_id)
                }
                return jsonify(response), 400

        trainee_data = generate_trainee_data(data)

        # with open(os.path.join(app.config["JSON_PATH"], f"{trainee_id}.json"), 'w') as trainee_file:
        #     json.dump(trainee_data, trainee_file, indent=4)
        response = {
            "status": "success",
            "message": "Portfolio for trainee_id: {0} is generated successfully".format(trainee_id),
            "portfolio_url" : "https://portfolios.talentsprint.com/{0}/{1}".format(data.get("program_code"), trainee_id)
        }
        return jsonify(response), 200

    elif trainee_id:
        trainee_data = get_trainee_data(trainee_id)

        if not trainee_data:
            response = {
                "status": "Failure",
                "message": "Trainee data for trainee_id: {0} does not exist".format(trainee_id)
            }
            return jsonify(response), 404

        return jsonify(trainee_data), 200

@app.route('/certificates/<rid>')
def certificates(rid):
        rid = str(rid)
        return render_template('resize.html')

@app.route('/getPortfolioFields', methods=["POST"])
def getPortfolioFields():
    try:
        data = request.form.to_dict()
        app.logger.info({'request_url': request.url, 'template_data': data})
        return {"listOfFields": app.config["portfolioFields"][data["portfolioType"]]}

    except Exception as err:
        app.logger.info({'request_url': request.url, "exceptionMessage": traceback.format_exc()})
        return {"listOfFields": list()}

@app.route('/<portfolioType>/sample')
def getPortfolioSample(portfolioType):
    return render_template('{0}.html'.format(portfolioType), data = app.config["portfolioSampleData"], isSample = True)

if __name__ == '__main__':
    #app.run()
    default_port = 6020
    parser = optparse.OptionParser()
    parser.add_option("-P", "--port",
         help="Port for the Flask app " + "[default %s]" % default_port,
            default=default_port)
    options, _ = parser.parse_args()
    app.run(port=int(options.port))
