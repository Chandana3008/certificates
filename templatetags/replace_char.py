from django import template

register = template.Library()

@register.filter
def replace_char(value):
    return value.replace("&","").lower()
