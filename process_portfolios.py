import re
import sys
import datetime
from re import search
import xlrd
import codecs
import json
import glob

dest_certificate_path = '/home/cdn/public_html/portfolios/certificates/'
dest_profile_image_path = '/home/cdn/public_html/portfolios/profile_images/'
dest_about_program_image_path = '/home/cdn/public_html/portfolios/about_program/'
dropbox_path = "/home/cdn/portfolios_dropbox/"
json_files_path = '/home/cdn/public_html/portfolios/profile_json/'
columns = ['traineeID', 'name', 'designation', 'company_name', 'program_name', 'cohort#', 'description']


def process_input(input_data):
    print("Process started : " + str(datetime.datetime.now()))
    try:
        sheet = xlrd.open_workbook(input_data)
        record = sheet.sheet_by_index(0)
        for row in range(record.nrows):
            if row == 0 :
                continue
            values = record.row_values(row)
            file_name = str(values[0]).split('.')[0]
            program_code = "DHAI"
            print(values[0])
            certificate_path = dropbox_path +'Certificates/'+ program_code + "/" + file_name + '.*'
            profile_path = glob.glob(dropbox_path + 'Photos/' + program_code + "/" + file_name + '.*')
            about_program_image_path = dropbox_path + 'About Program/' + values[len(values) - 1]
            print(about_program_image_path)
            if not os.path.exists(about_program_image_path):
                print("About Program image doesn't exist")
                continue

            if not glob.glob(certificate_path):
                print("Certificate doesn't exists for " + file_name + ".")
                continue
            if not profile_path:
                print(dropbox_path + 'Photos/' + program_code + "/" + file_name + '.*')
                print("Profile Image doesn't exists for " + file_name + ".")
                continue
            excel_to_json(values)
            certificate_conversion(glob.glob(certificate_path)[0], program_code, file_name, values[len(values) - 1])
    except Exception as err:
        print("Please upload correct worksheet" + err)
    print("Process ended : " + str(datetime.datetime.now()))

def excel_to_json(values):
        data = {}
        for i in range(0, len(columns)):
            if i == 0:
                values[i] = str(values[0]).split('.')[0]
            data[columns[i]] = values[i]
            project_list, project_dict, about_program, about_program_image = get_projects(values)
            data['project_list'] = project_list
            data['project_description'] = project_dict
            data['about_program'] = about_program
            data['program_code'] = "DHAI"
            data['about_program_image'] = about_program_image
            if str(values[0]).split('.')[0] == '2001412':
                data['file_name'] = str(values[0]).split('.')[0] + '_latest.jpg'
        save_json(data)


def get_projects(values):
    project_list = []
    project_dict = {}
    about_program = ""
    for j in range(6, len(values) - 2, 2):
        if values[j] == "NA":
            continue
        if values[j] == "":
            return project_list, project_dict, about_program
        split_projects = str(values[j]).split("\n")
        if len(split_projects) > 1:
            project_title = "<h3>" + split_projects[0] + "</h3><h3>" + split_projects[1] + "</h3>"
        else :
            project_title = values[j]
        project_list.append(project_title)
        split_values =  str(values[j + 1]).split("\n\n")
        if len(split_values) == 1:
            project_dict[project_title] = "<p class = \"byline\">" + str(values[j + 1]) + "</p>"
        else:
            project_dict[project_title] = "<p class = \"byline\">" + split_values[0] + "</p><p class = \"byline\">" + split_values[1]  + "</p>"

    about_program_split =  values[len(values) - 2].split("\n\n")
    if len(about_program_split) > 1:
        about_program = "<p class = \"byline-about\">" +about_program_split[0]+ "</p><p class = \"byline-about\">" +about_program_split[1]+ "</p>"
    elif about_program_split:
        about_program  = "<p class = \"byline-about\">" +about_program_split[0]+ "</p>"
    else:
        about_program = ""
    about_program_image = values[len(values) - 1]
    return project_list, project_dict, about_program, about_program_image
def certificate_conversion(path, program_code, file_name, about_program_image):
    print(path)
    try:
        if ".pdf" in path:
            images = convert_from_path(path, first_page = 0, last_page = 1)
            images[0].save(dest_certificate_path + program_code + "/" + file_name + ".jpg")
        else:
            copyfile(path, dest_certificate_path + program_code + "/" + file_name + ".jpg")
        print("Certificate copied successfully.")
        copyfile(glob.glob(dropbox_path+'Photos/' + program_code + "/" + file_name + '.*')[0], dest_profile_image_path +  program_code + "/" + file_name + ".png")
        print("Copied profile images to archives successfully.")
        copyfile(dropbox_path + "About Program/" + about_program_image, dest_about_program_image_path + about_program_image)
        print("Copied About program image successfully.")
    except Exception as err:
        print("Error in copying certificate file " + str(err))

def save_json(data):
    json.dumps(data)
    json_path = json_files_path + data['traineeID'] + '.json'
    with open(json_path, "w") as output_file:
        json.dump(data, output_file, indent = 4, ensure_ascii = False)
    print("JSON file created Successfully.")


def process_filename(path, trainee_id):
    for file in os.listdir('/home/cdn/portfolios_dropbox/photos/DHAI/'):
        if file.split('.')[0] == trainee_id:
            print(file)
            break
    return file

if __name__ == "__main__":
    print(sys.argv[1])
    process_input(sys.argv[1])